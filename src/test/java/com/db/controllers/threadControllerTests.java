package com.db.controllers;

import com.hw.db.controllers.threadController;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Thread;
import com.hw.db.models.Vote;
import com.hw.db.models.User;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.hw.db.DAO.ThreadDAO;

import java.util.Collections;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


class threadControllerTests {
    private String mockSlug = "slug";
    private Thread thread;


    @BeforeEach
    @DisplayName("Thread controller tests set up")
    void beforeThreadTests() {
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());

        thread = new Thread("author", ts, "forum", "message", mockSlug, "title", 0);
    }


    @Test
    @DisplayName("Testing CheckIdOrSlug method (with different kind of inputs)")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt("0"))).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(thread, controller.CheckIdOrSlug(mockSlug), "Found thread by slug");
            assertEquals(thread, controller.CheckIdOrSlug("0"), "Found thread by id");

            assertNull(controller.CheckIdOrSlug("1"), "Not found slug");
            assertNull(controller.CheckIdOrSlug("slug123"), "Not found id");
        }
    }

    @Test
    @DisplayName("Testing createPost method (successful case)")
    void testCreatePost() {
        List<Post> posts = Collections.emptyList();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic userDAO = Mockito.mockStatic(UserDAO.class)) {
                threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(thread);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), controller.createPost(mockSlug, posts), "Post was successfully created");
            }
        }
    }

    @Test
    @DisplayName("Testing Posts method (successful case)")
    void testPosts() {
        List<Post> posts = Collections.emptyList();;
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(thread);

            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), controller.Posts(mockSlug, 5, 1, "tree", true), "Posts were successfully retrieved");
        }

    }

    @Test
    @DisplayName("Testing change method (NOT_FOUND case)")
    void testChange() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(null);
            threadMock.when(() -> ThreadDAO.change(null, thread)).thenThrow(new DataAccessException("Раздел не найден."){});
            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).body(new com.hw.db.models.Message("Раздел не найден.")).getStatusCode(), controller.change(mockSlug, thread).getStatusCode(), "Thread is no found");
        }
    }

    @Test
    @DisplayName("Testing info method (successful case)")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(thread);

            threadController controller = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), controller.info(mockSlug), "Info was successfully retrieved");
        }
    }

    @Test
    @DisplayName("Testing createVote method (successful case)")
    void testCreateVote() {
        User user = new User("nickname", "email", "fullname", "about");
        Vote vote = new Vote("nickname", 5);
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {

                threadMock.when(() -> ThreadDAO.getThreadBySlug(mockSlug)).thenReturn(thread);
                userMock.when(() -> UserDAO.Info("nickname")).thenReturn(user);

                threadController controller = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(), controller.createVote(mockSlug, vote).getStatusCode(), "Vote was successfully created");
            }
        }
    }

}
